"""
Unit tests for flatpack.session
"""
import json

import requests
import mock
from flatpack import flatconfig
from flatpack.session import FlatSession, DEFAULT_SERVER
from test_flatconfig import SetterDict


def make_session_cfg(cmdr=None, apikey=None, server=None):
    """
    Make a session config store
    :param cmdr:
    :param apikey:
    :param server:
    :return:
    """
    configs = SetterDict()
    if cmdr:
        configs["cmdrs"] = [cmdr]
    store = flatconfig.Config(configs)
    if server:
        store[flatconfig.SERVER] = server
    if apikey:
        store[flatconfig.APIKEY] = apikey

    return store


def test_flatsession_configure():
    """
    Test that we can create a FlatSession and initialize it from config values
    :return:
    """
    cfg = make_session_cfg("fred", server="http://blaa", apikey="1234")
    session = FlatSession()
    session.configure(cfg)
    assert session.cmdr == "fred"
    assert session.apikey == "1234"
    assert session.server == "http://blaa"


def test_flatsession_reconfigure():
    """
    Test that we can create a FlatSession and re-initialize it from config values
    :return:
    """
    cfg = make_session_cfg("fred", server="http://blaa", apikey="1234")
    session = FlatSession()
    session.configure(cfg)
    assert session.cmdr == "fred"
    assert session.apikey == "1234"
    assert session.server == "http://blaa"

    cfg[flatconfig.APIKEY] = "7777"
    session.reconfigure()
    assert session.apikey == "7777"


def test_flatsession_configure_default():
    """
    Test that we can create a FlatSession set the default server
    :return:
    """
    cfg = make_session_cfg("fred", apikey="1234")
    session = FlatSession()
    session.configure(cfg)

    assert session.server == DEFAULT_SERVER


def make_mock_response(status=200, cmdrs=["fred"], week_contrib=0, influence_count=0):
    """
    Make a mock response object
    :param status:
    :param cmdrs:
    :param week_contrib:
    :param influence_count:
    :return:
    """
    response = mock.Mock()
    response.status_code = status
    response.content = json.dumps(
        {
            "commanders": cmdrs,
            "contributions":  {
                "current_week": {
                    "total_value": week_contrib,
                    "influence_updates": {
                        "count": influence_count
                    }
                }
            },

        })
    return response


def test_flatsession_get_profile(monkeypatch):
    """
    Test the get_profile() method
    :return:
    """
    response = make_mock_response(status=200, cmdrs=["fred"])
    mock_requests_get = mock.Mock(return_value=response)
    monkeypatch.setattr(requests, "get", mock_requests_get)

    cfg = make_session_cfg("fred", apikey="12345678")
    session = FlatSession()
    session.configure(cfg)

    data = session.get_profile("fred")
    assert "contributions" in data


def test_flatsession_postevent(monkeypatch):
    """
    Test that when we submit an event we update our commander data after
    :param monkeypatch:
    :return:
    """
    mock_display = mock.Mock()
    response = make_mock_response(status=200, cmdrs=["fred"], week_contrib=3333, influence_count=12)
    monkeypatch.setattr(requests, "get", mock.Mock(return_value=response))
    monkeypatch.setattr(requests, "post", mock.Mock(return_value=make_mock_response(200)))

    cfg = make_session_cfg("fred", apikey="12345678")
    session = FlatSession()
    session.display = mock_display
    session.configure(cfg)
    assert session.uploads == 0
    assert not mock_display.called
    session._journal_entry("fred", "Lave", "Lave Station", {"event": "FSDJump"}, "state")
    assert session.uploads == 1
    assert mock_display.called


def test_flatsession_bad_apikey(monkeypatch):
    """
    Test that when we submit an event with a bad api key we don't crash
    :param monkeypatch:
    :return:
    """
    response = make_mock_response(status=401, cmdrs=["fred"], week_contrib=3333, influence_count=12)
    monkeypatch.setattr(requests, "get", mock.Mock(return_value=response))
    monkeypatch.setattr(requests, "post", mock.Mock(return_value=response))

    cfg = make_session_cfg("fred", apikey="12345678")
    session = FlatSession()
    session.configure(cfg)
    assert session.contribs == 0
    session.journal_entry("fred", "Lave", "Lave Station", {"event": "FSDJump"}, "state")
    assert session.contribs == 0

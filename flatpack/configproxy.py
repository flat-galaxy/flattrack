try:
    from config import config
except ImportError:
    class TestConfig(dict):
        def get_str(self, key, default=None):
            return self.get(key, default)
    config = TestConfig()

"""
The main object for interacting with the FGS server
"""
import json
import threading
import time
import traceback
import requests
import uuid

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    from urllib import quote as url_quote
except ImportError:
    from urllib.parse import quote as url_quote

from . import overlayclient
from . import flatconfig
from . import worker
from .logger import LOG


IDENT = "edmc-FlatTrack"
VERSION = "0.7.2"
HTTP_HEADERS = {
    "user-agent": IDENT + "_" + VERSION
}
DEFAULT_SERVER = "https://ui.flatgalaxysociety.space"

POOL = worker.Pool(5, LOG)


def shorten(text):
    """
    Shorten some BGS UI strings
    :param text:
    :return:
    """
    if text:
        text = text.replace("count double", "2x")
        text = text.replace("All actions count", "All acts count")
    else:
        text = ""
    return text


def shorten_faction(current_system, faction_name):
    """
    Strip out system name from faction name
    :param current_system:
    :param faction_name:
    :return:
    """
    if current_system in faction_name:
        return faction_name.replace(current_system, "").strip()
    return faction_name


class FlatSession(object):
    """
    Maintain an authenticated session and provide actions
    """

    def __init__(self):
        super(FlatSession, self).__init__()
        self.server = None
        self.apikey = None
        self.cmdr = None
        self.password = None
        self.config = None
        self.uploads = 0
        self.contribs = 0
        self.overlay_id = IDENT
        self.last_http_result = None
        self.task_list_widget = None
        self.tasks = []
        self.current_system = None
        self.current_station = None
        self.cargo_list_widget = None
        self.overlay_lock = threading.Lock()
        self.tasks_lock = threading.Lock()

        self.task_msg = ""

        self.current_tasks = {}

        POOL.begin(self.update_loop)
        POOL.begin(self.overlay_alive)

    def update_loop(self):
        """
        Every 2 mins update the tasks list
        :return:
        """
        # wait for the gui to be ready
        while not self.task_list_widget:
            time.sleep(3)

        while True:
            self._set_tasks()
            time.sleep(60 * 2)

    def set_tasks(self):
        """
        Update the task list (non-blocking)
        :return:
        """
        POOL.begin(self._set_tasks)

    def _set_tasks(self):
        """
        Update the task list gui from the server
        :return:
        """
        LOG.write("update tasks display..")
        tasks = self.get_tasks()
        if tasks:
            self.tasks = tasks

        if self.tasks:
            LOG.write("update {} tasks..".format(len(self.tasks)))
            if self.current_system:
                LOG.write(" current system {}".format(self.current_system))
                # find tasks in this system
                here = []
                for t in self.tasks:
                    if t.get("system_name", "") == self.current_system:
                        here.append(t)

                def render_tasks_list():
                    with self.tasks_lock:
                        msg = ""
                        self.current_tasks = {}
                        self.task_list_widget.configure(state=tk.NORMAL)
                        self.task_list_widget.delete(1.0, tk.END)  # empty the text widget
                        self.task_list_widget.configure(state=tk.DISABLED)
                        for task in here:
                            task_descrip = shorten(task["primary_active_state"]["description"])
                            if not task_descrip:
                                task_descrip = "No specific orders"

                            details = self.format_task_details(task)
                            ports = self.format_task_stations(task)
                            contribs = self.format_task_contributors(task)
                            state = task["primary_active_state"]["name"]

                            task_stations = []
                            for station in task["stations"]:
                                task_stations.append("{} [{}]\n".format(
                                    station["name"], station["max_landing_pad"]))

                            self.current_tasks[details] = {
                                task_descrip: task_stations
                            }

                            self.task_list_widget.configure(state=tk.NORMAL)
                            self.task_list_widget.insert(tk.END, details, "bold")
                            self.task_list_widget.insert(tk.END, "[ State : {} ]\n".format(state), "center")
                            self.task_list_widget.insert(tk.END, ports)
                            self.task_list_widget.update()
                            self.task_list_widget.configure(state=tk.DISABLED)

                            msg = "{}{}{}".format(details, ports, contribs)
                        if not msg:
                            msg = "- no tasks in this system -"
                        self.task_msg = msg

                if self.task_list_widget:
                    self.task_list_widget.after(1, render_tasks_list)

    def overlay_alive(self):
        """
        Display a small animation all the time
        :return:
        """
        animation = [
            "o---",
            "-o--",
            "--o-",
            "---o",
            "--o-",
            "-o--",
        ]
        i = 0
        while 1:
            time.sleep(1)
            with self.overlay_lock:
                overlayclient.display("flat-busy", animation[i], x=1200, y=0, ttl=2, color="#cc9900", size="normal")
                i = (i + 1) % len(animation)

    def overlay_task_msg(self):
        """
        Render self.task_msg
        :return:
        """

        with self.tasks_lock:
            self.task_msg = ""
            for place in self.current_tasks:
                self.task_msg += place
                for job in self.current_tasks[place]:
                    self.task_msg += "\n" + job

        self.overlay_display_lines("FlatTrack ->", y=0, color="#ff9900", size="large")
        self.overlay_display_lines(self.task_msg, y=24)

    def overlay_display_lines(self, msg, x=0, y=0, color="#bbbbbb", size=None):
        """
        Render several lines with the overlay
        :param x: top left
        :param y: top
        :param msg: multi-line string to display
        :param color: hex color code
        :param size:
        :return:
        """

        lines = msg.splitlines()
        for line in lines:
            if line:
                with self.overlay_lock:
                    overlayclient.display(str(uuid.uuid4()), line, x=x, y=y, ttl=10, color=color, size=size)
                    time.sleep(0.2)
                    y += 21

    def format_task_contributors(self, task):
        team = task["contributions"]
        msg = ""
        if team:
            msg += " -- Contributors:\n"

            cmdrs = set()

            for contrib in team:
                name = contrib["commander_name"]
                if name:
                    cmdrs.add(name)
            for name in cmdrs:
                msg += " " * 6 + "{}\n".format(name)
        return msg

    def format_task_stations(self, task):
        msg = ""
        if task["stations"]:
            for station in task["stations"]:
                msg += " > {} [{}]\n".format(
                    station["name"], station["max_landing_pad"])
        return msg

    def format_task_details(self, task):
        msg = "{} ({})\n".format(task["system_name"],
                                 shorten_faction(self.current_system,
                                                 task["faction_name"]))
        return msg

    def configure(self, config):
        """
        Load our session settings from a flatconfig.Config object
        :param flatconfig.Config config:
        :return:
        """
        LOG.write("loading configuration")

        self.config = config
        if config is not None:
            if not config[flatconfig.SERVER]:
                config[flatconfig.SERVER] = DEFAULT_SERVER
            self.server = config[flatconfig.SERVER]
            self.apikey = config[flatconfig.APIKEY]

            LOG.write("server is {}".format(self.server))

            cmdrs = config.get_global("cmdrs")
            if cmdrs is not None:
                if len(cmdrs):
                    self.cmdr = cmdrs[0]
            LOG.write("cmdr is {}".format(self.cmdr))

    def reconfigure(self):
        """
        Re-load our configs
        :return:
        """
        self.configure(self.config)

    def display(self, message, **kwargs):
        """
        Display a HITS message
        :param message:
        :return:
        """
        overlayclient.display(self.overlay_id, message, **kwargs)

    def get_tasks(self):
        """
        Get the tasks for the current user
        :return:
        """
        if not self.apikey or not self.server:
            LOG.write("no apikey or server, skipping tasks")
            return None


        paths = [self.server.rstrip("/"),
                 'api',
                 self.apikey,
                 'tasks']
        url = "/".join(paths)
        if self.current_system:
            url += "?system=" + self.current_system
        resp = requests.get(url, headers=HTTP_HEADERS)
        if resp.status_code == 200:
            data = json.loads(resp.content)
            return data
        return None

    def get_profile(self, cmdr):
        """
        Get the current player's standing
        :param cmdr:
        :return:
        """
        if not cmdr or not self.apikey or not self.server:
            return None

        paths = [self.server.rstrip("/"),
                 'api',
                 self.apikey]
        url = "/".join(paths)

        resp = requests.get(url, headers=HTTP_HEADERS)
        if resp:
            LOG.write("server responded to api key with {}".format(resp.status_code))
            if resp.status_code == 200:
                data = json.loads(resp.content)
                if data and "commanders" in data:
                    if cmdr in data["commanders"]:
                        return data
        return None

    def post_event(self, entry, cmdr, system, station):
        """
        process a journal even and perhaps Send it to the FGS server
        :param station:
        :param system:
        :param cmdr:
        :param entry:
        :return:
        """
        LOG.write("got event: type: {} cmdr: {} system: {}".format(entry["event"], cmdr, system))

        if not cmdr:
            return
        if not system:
            return

        paths = [self.server.rstrip("/"),
                 'api',
                 self.apikey,
                 url_quote(cmdr),
                 url_quote(system)]

        if station is not None:
            paths.append(station)
        url = "/".join(paths)

        try:
            updated = False
            if entry and len(entry):
                LOG.write("posting event: {} for system {} from {}".format(
                    entry["event"], system, cmdr))
                form_encoded = {"event": json.dumps(entry)}
                resp = requests.post(url, data=form_encoded, headers=HTTP_HEADERS)
                if resp:
                    LOG.write("event post response was {}".format(resp.status_code))
                    if resp.status_code == 200:
                        updated = True
            if updated:
                self.display("BGS update sent..")
                self.uploads += 1
        except Exception as err:
            LOG.write(traceback.format_exc())
            self.last_http_result = str(err)

    def cmdr_data(self, data):
        """
        Process new CAPI data (non-blocking)
        :param data:
        :return:
        """
        POOL.begin(self._cmdr_data, data)

    def _cmdr_data(self, data):
        """
        Handle cmdr CAPI data (blocking)
        :param data:
        :return:
        """
        if not data["commander"]["alive"]:
            return

        if not self.current_system:
            self.current_system = data["lastSystem"]["name"]
        if not self.current_station:
            if data["commander"]["docked"]:
                self.current_station = data["lastStarport"]["name"]

        self.set_tasks()

    def journal_entry(self, cmdr, system, station, entry, state):
        """
        Handle journal updates (non-blocking)
        :param cmdr:
        :param system:
        :param station:
        :param entry:
        :param state:
        :return:
        """
        POOL.begin(self._journal_entry, cmdr, system, station, entry, state)

    def _handle_text(self, cmdr, system, station, entry):
        """
        Handle a chat event
        :param cmdr:
        :param system:
        :param station:
        :param entry:
        :return:
        """
        if entry["Message"] == "!tasks":
            self.overlay_task_msg()

    def _journal_entry(self, cmdr, system, station, entry, state):
        """
        Process a journal item (blocking)
        :param cmdr:
        :param system:
        :param station:
        :param entry:
        :param state:
        :return:
        """

        if "event" not in entry:
            return

        if entry["event"] == "SendText":
            self._handle_text(cmdr, system, station, entry)
            return

        location_changed = self.current_system != system or self.current_station != station

        if location_changed:
            self.current_station = station
            self.current_system = system
            self.set_tasks()

            POOL.begin(self.overlay_task_msg)

        self.post_event(entry, cmdr, system, station)

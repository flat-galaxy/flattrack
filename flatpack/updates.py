"""
Check for updates of flattrack
"""
import re
import requests
from .session import VERSION

match_ver = re.compile(r"(\d+)\.(\d+)\.(\d+)$")


def str_to_version(ver):
    check = match_ver.search(ver)
    if check:
        major, minor, patch = int(check.group(1)), int(check.group(2)), int(check.group(3))
        return major, minor, patch
    return None, None, None


V_MAJOR, V_MINOR, V_PATCH = str_to_version(VERSION)


def version_is_newer(ver):
    if V_MAJOR is not None:
        major, minor, patch = str_to_version(ver)
        if major is not None:
            if major > V_MAJOR:
                return True
            if major == V_MAJOR:
                if minor > V_MINOR:
                    return True
                elif minor == V_MINOR:
                    return patch > V_PATCH
    return False


def check_for_updates():
    url = "https://gitlab.com/api/v4/projects/4404416/releases"
    resp = requests.get(url)
    if resp.status_code == 200:
        data = resp.json()
        for release in data:
            tag = release["tag_name"]
            if version_is_newer(tag):
                return release

    return None


if __name__ == "__main__":
    print(check_for_updates())

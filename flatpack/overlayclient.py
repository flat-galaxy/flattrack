"""
Really basic abstraction for HITS Overlay if installed
"""

from .logger import LOG
import traceback


SEEN_ERRORS = set()


def display(sender, message, x=50, y=1100, ttl=6, color="#339933", size="normal"):
    """
    If installed, display a message on EDMCOverlay/HITS
    :param size:
    :param color:
    :param sender:
    :param ttl:
    :param y:
    :param x:
    :param message:
    :return:
    """

    if display.overlay is False:  # not installed, do nothing
        return

    try:
        if display.overlay is None:
            LOG.write("Attempting to contact EDMCOverlay..")
            from EDMCOverlay import edmcoverlay
            LOG.write("EDMCOverlay module loaded..")
            display.overlay = edmcoverlay.Overlay()
            LOG.write("EDMCOverlay connected..")
        display.overlay.send_message(sender, message, color, x, y, ttl, size=size)
    except ImportError:
        display.overlay = False  # not installed
        LOG.write("EDMCOverlay package not installed")
    except Exception:
        error = traceback.format_exc()
        if error not in SEEN_ERRORS:
            SEEN_ERRORS.add(error)
            LOG.write("Error talking to EDMCOverlay")
            LOG.write(error)


display.overlay = None

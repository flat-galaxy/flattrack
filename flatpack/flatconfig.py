"""
Simple abstraction for read/write of configuration for our plugin
"""
from .configproxy import config

WRITE_PREFIX = "FlatTrack_"
READ_PREFIXES = [WRITE_PREFIX, "NULLTracker"]

SERVER = "Server_Address"
APIKEY = "APIKey"
CMDR = "CMDR"
PASSWORD = "Password"


class Config(object):
    """
    Basic config store/proxy
    """
    def __init__(self, store=config):
        self.config = store

    def get_global(self, item):
        """
        Get a global level config value
        :return:
        """
        try:
            return self.config.get_str(item)
        except ValueError:
            return self.config.get_list(item)

    def __getitem__(self, item):
        """
        Retrieve a config item
        :param item:
        :return:
        """
        for prefix in READ_PREFIXES:
            value = self.get_global(prefix + item)
            if value is not None:
                return value
        return None

    def __setitem__(self, key, value):
        """
        Write a config change
        :param key:
        :param value:
        :return:
        """
        self.config.set(WRITE_PREFIX + key, value)

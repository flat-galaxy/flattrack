"""
Test some of the HITS interface
"""

import mock

from flatpack import overlayclient


def setup_function():
    """
    Make the overlay client clean
    :return:
    """
    overlayclient.display.overlay = None


def test_display():
    """
    Test the default method doesn't throw
    :return:
    """
    overlayclient.display("fred", "hello")


def test_call_send_message():
    """
    Test that if we create a fake display overlay, that we call the send_message method
    :return:
    """
    assert overlayclient.display.overlay is None
    overlay = mock.Mock()
    overlay.send_message = mock.Mock()
    overlayclient.display.overlay = overlay

    overlayclient.display("fred", "bob")
    assert overlay.send_message.called

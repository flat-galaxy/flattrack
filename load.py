"""
The FGS Tracker Plugin

(c) 2017 Ian Norton,  BSD License
"""
from __future__ import print_function
import os
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk
import sys
import myNotebook as nb
from monitor import monitor
from ttkHyperlinkLabel import HyperlinkLabel

from flatpack.session import FlatSession, VERSION, POOL, IDENT
from flatpack import flatconfig, updates
from flatpack.logger import LOG

OURDIR = os.path.dirname(os.path.abspath(__file__))
OURNAME = os.path.basename(OURDIR)
PLUGINS = os.path.dirname(OURDIR)

CONFIG = flatconfig.Config()
STATUS = ""
UPLOADS = ""

PREF_SERVER = tk.StringVar(value=CONFIG[flatconfig.SERVER])
PREF_APIKEY = tk.StringVar(value=CONFIG[flatconfig.APIKEY])
PREF_STATUS = tk.StringVar(value="")
PREF_UPLOADS = tk.StringVar(value="")

SESSION = FlatSession()

WARNINGS = []


def plugin_start():
    """
    Start up our EDMC Plugin
    :return:
    """
    LOG.rotate_log()
    LOG.write("Starting {} {}".format(IDENT, VERSION))
    print("Recording {} log to {}".format(IDENT, LOG.filename))

    other_plugins = [x for x in os.listdir(PLUGINS) if x != OURNAME and os.path.isdir(os.path.join(PLUGINS, x))]
    flat_tracks = [x for x in other_plugins if x.lower().startswith("flattrack")]
    if len(flat_tracks):
        WARNINGS.append("Remove old flattrack plugins!")

    if not WARNINGS:
        SESSION.configure(CONFIG)
        POOL.begin(_plugin_started)


def plugin_start3(plugindir):
    plugin_start()


def _plugin_started():
    profile = SESSION.get_profile(SESSION.cmdr)
    if profile:
        PREF_STATUS.set(SESSION.contribs)
        PREF_UPLOADS.set(SESSION.uploads)


def plugin_prefs(parent, cmdr, isbeta):
    global CONFIG
    frame = nb.Frame(parent)
    frame.columnconfigure(1, weight=1)

    nb.Label(frame, text="Flat Track Configuration").grid(padx=10, row=8, sticky=tk.W)

    nb.Label(frame, text="Server             ").grid(padx=10, row=10, sticky=tk.W)
    nb.Entry(frame, textvariable=PREF_SERVER).grid(padx=10, row=10, column=1, sticky=tk.EW)

    nb.Label(frame, text="API Key            ").grid(padx=10, row=11, sticky=tk.W)
    nb.Entry(frame, textvariable=PREF_APIKEY).grid(padx=10, row=11, column=1, sticky=tk.EW)

    nb.Label(frame, text="Status             ").grid(padx=10, row=12, sticky=tk.W)
    nb.Label(frame, textvariable=PREF_STATUS).grid(padx=10, row=12, column=1, sticky=tk.EW)
    nb.Label(frame, textvariable=PREF_UPLOADS).grid(padx=10, row=13, column=1, sticky=tk.EW)

    nb.Label(frame, text="Version            ").grid(padx=10, row=14, sticky=tk.W)
    nb.Label(frame, text=VERSION).grid(padx=10, row=14, column=1, sticky=tk.EW)

    return frame


def prefs_changed(cmdr, is_beta):
    CONFIG[flatconfig.SERVER] = PREF_SERVER.get()
    CONFIG[flatconfig.APIKEY] = PREF_APIKEY.get()


def cmdr_data(data, is_beta):
    """
    CAPI data arrived!
    :param data:
    :param is_beta:
    :return:
    """
    SESSION.cmdr_data(data)


def journal_entry(cmdr, _, system, station, entry, state):
    """
    Make sure the service is up and running
    :param cmdr:
    :param system:
    :param station:
    :param entry:
    :param state:
    :return:
    """
    if monitor.is_live_galaxy():
        SESSION.journal_entry(cmdr, system, station, entry, state)


this = sys.modules[__name__]  # For holding module globals


def plugin_app(parent):
    """
    Create a pair of TK widgets for the EDMC main window
    """
    latest = ""
    release = updates.check_for_updates()
    if not release:
        latest = " (latest)"
    header = "-=#@ Flat Track {}{} @#=-".format(VERSION, latest)
    frame = tk.Frame(parent)

    label = tk.Label(
        frame,
        text=header,
        justify=tk.CENTER)

    label.grid(row=0, column=0, sticky=tk.NSEW)
    task_height = 10
    initial_tasks_txt = "loading tasks.\n\nPlease wait..\n"
    if WARNINGS:
        initial_tasks_txt = "\n".join(WARNINGS)
        task_height = 2
    this.tasksheet = tk.Text(
        frame,
        height=task_height,
        width=40,
        borderwidth=2,
        relief="flat",
        font=("Courier New", "8"))

    this.tasksheet.grid(row=1, column=0, sticky=tk.W)
    this.tasksheet.insert(tk.END, initial_tasks_txt)
    this.tasksheet.tag_configure("bold", font=("Courier New", "9", "bold"))
    this.tasksheet.tag_configure(tk.CENTER, justify=tk.CENTER)
    this.tasksheet.configure(state=tk.DISABLED)
    this.tasksheet.configure({
        "fg": label.cget("fg"),
        "bg": label.cget("bg")
    })

    if release:
        url = "https://gitlab.com/flat-galaxy/flattrack/-/archive/{}/flattrack-{}.zip".format(
            release["tag_name"],
            release["tag_name"]
        )
        update_link = HyperlinkLabel(
            frame,
            text=">> FlatTrack {} Available! <<".format(release["tag_name"]),
            justify=tk.CENTER,
            url=url)
        update_link.grid(row=2, column=0, sticky=tk.EW)

    SESSION.task_list_widget = this.tasksheet

    parent.after(500, SESSION.get_tasks)

    return frame


def plugin_stop():
    """
    EDMC is shutting down
    :return:
    """
    LOG.write("EDMC Exiting..")
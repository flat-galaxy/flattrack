"""
Basic level tests for flatconfig
"""

from flatpack import flatconfig


class SetterDict(dict):
    def set(self, key, value):
        self[key] = value

    def get_str(self, key, default=None):
        return self.get(key, default)


def test_config_getset():
    """
    Test simple get and set
    :return:
    """
    data = SetterDict()
    cfg = flatconfig.Config(store=data)

    assert cfg["foo"] is None
    cfg["foo"] = "hello"
    assert cfg["foo"] == "hello"
